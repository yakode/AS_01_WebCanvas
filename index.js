const canvas = document.getElementById('myCanvas');
const ctx = canvas.getContext('2d');

const width = canvas.width;
const height = canvas.height;

let imgData;
let saveImg = [];
let saveIndex = -1;
saveCanvas();

let mode = "pencil";
let color = "#000000";
let font = "Consolas";
let x1 = 0, y1 = 0;
let x2 = 0, y2 = 0;


let textBox = document.getElementById("textBox");
let textContent = "";
textBox.hidden = true;

canvas.style.cursor = "url('./img/cursor_pencil.png'), pointer";

let isPainting = false;
let textFlag = false;

canvas.addEventListener('mousedown', function (e) {
    isPainting = true;

    imgData = ctx.getImageData(0, 0, width, height);

    x1 = getMouseX(e);
    y1 = getMouseY(e);
    ctx.globalCompositeOperation = 'source-over';
    ctx.lineWidth = document.getElementById("cuxiBar").value;
    ctx.strokeStyle = color;
    ctx.fillStyle = color;

    textBox.hidden = true;
    switch (mode) {
        case "pencil":
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            break;
        case "eraser":
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.globalCompositeOperation = "destination-out";
            break;
        case "text":
            textBox.hidden = false;
            if (textFlag) {
                textContent = textBox.value;
                textFlag = false;
                textBox.style['z-index'] = 1;
                textBox.value = "";
                drawing(e);
            } else if (!textFlag) {
                textFlag = true
                textBox.style.left = x1 + 'px';
                textBox.style.top = y1 + 'px';
                textBox.style['z-index'] = 6;
            }
            break;
        default:
    }


})

canvas.addEventListener('mousemove', function (e) {
    if (!isPainting) return;
    x2 = getMouseX(e);
    y2 = getMouseY(e);

    switch (mode) {
        case "pencil":
            ctx.lineTo(x2, y2);
            ctx.stroke();
            break;
        case "eraser":
            ctx.lineTo(x2, y2);
            ctx.stroke();
            break;
        case "straight":
            clearCanvas(0);
            ctx.putImageData(imgData, 0, 0);

            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.closePath();
            ctx.stroke();
            break;
        case "rectangle":
            clearCanvas(0);
            ctx.putImageData(imgData, 0, 0);

            ctx.strokeRect(x1, y1, x2 - x1, y2 - y1);
            break;
        case "triangle":
            clearCanvas(0);
            ctx.putImageData(imgData, 0, 0);

            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.lineTo(x2 - 2 * (x2 - x1), y2);
            ctx.closePath();
            ctx.stroke();
            break;
        case "circle":
            clearCanvas(0);
            ctx.putImageData(imgData, 0, 0);

            ctx.beginPath();
            ctx.arc(x1, y1, Math.abs(x2 - x1), 0, 2 * Math.PI);
            ctx.stroke();
            break;
        default:
    }


})

canvas.addEventListener('mouseup', function (e) {
    isPainting = false;
    saveCanvas();
})


//mouse position
function getMouseX(event) {
    var rect = canvas.getBoundingClientRect();
    return event.clientX - rect.left;
}
function getMouseY(event) {
    var rect = canvas.getBoundingClientRect();
    return event.clientY - rect.top;
}


//click!
function clickBrush(brush) {
    mode = brush;
    textBox.value = "";
    textFlag = false;
    isPainting = false;
    switch (brush) {
        case "pencil":
            canvas.style.cursor = "url('./img/cursor_pencil.png'), pointer";
            break;
        case "eraser":
            canvas.style.cursor = "url('./img/cursor_eraser.png'), pointer";
            break;
        case "straight":
            canvas.style.cursor = "url('./img/cursor_straight.png'), pointer";
            break;
        case "rectangle":
            canvas.style.cursor = "url('./img/cursor_rectangle.png'), pointer";
            break;
        case "triangle":
            canvas.style.cursor = "url('./img/cursor_triangle.png'), pointer";
            break;
        case "circle":
            canvas.style.cursor = "url('./img/cursor_circle.png'), pointer";
            break;
        case "text":
            canvas.style.cursor = "text";
            break;
        default:
    }
}


//simple menu
var cuxiBar = document.getElementById('cuxiBar');
cuxiBar.addEventListener('change', cuxiChange, false);
function cuxiChange(e) {
    document.getElementById("cuxiValue").innerHTML = "Brush size: " + cuxiBar.value;
}
var textBar = document.getElementById('textBar');
textBar.addEventListener('change', textChange, false);
function textChange(e) {
    document.getElementById("textValue").innerHTML = "Text size: " + textBar.value;
}
var fontselect = document.getElementById('font');
fontselect.addEventListener('change', fontChange, false);
function fontChange(e) {
    font = document.getElementById('font').value;
}


//text
function drawing(e) {
    if (!ctx) {
        return;
    } else {
        ctx.save();
        ctx.beginPath();

        ctx.font = document.getElementById("textBar").value + "px " + font;
        ctx.fillText(textContent, parseInt(textBox.style.left), parseInt(textBox.style.top));

        ctx.restore();
        ctx.closePath();
    }
};


//undo, redo
function reNundo(doWhat) {
    switch (doWhat) {
        case "redo":
            if (saveImg.length == saveIndex + 1) return;
            saveIndex += 1;
            ctx.putImageData(saveImg[saveIndex], 0, 0);
            break;
        case "undo":
            if (saveIndex < 1) return;
            saveIndex -= 1;
            ctx.putImageData(saveImg[saveIndex], 0, 0);
            break;
    }
}
function saveCanvas() {
    saveIndex += 1;
    if (saveImg.length == saveIndex)
        saveImg.push(ctx.getImageData(0, 0, width, height));
    else {
        saveImg[saveIndex] = ctx.getImageData(0, 0, width, height);
        while (saveImg.length != saveIndex + 1) saveImg.pop();
    }
}


//download
function downloadImg() {
    var link = document.createElement('a');
    link.download = 'draw.png';
    link.href = canvas.toDataURL()
    link.click();
}


//upload
var imageLoader = document.getElementById('uploadImg');
imageLoader.addEventListener('change', uploadImg, false);
function uploadImg(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var img = new Image();
        img.onload = function () {
    		clearCanvas(0);
            ctx.drawImage(img, 0, 0);
            saveCanvas();
        };
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}


//clear
function clearCanvas(save) {
    ctx.clearRect(0, 0, width, height);
    if (save) saveCanvas();
}


//color selector
var colorCanvas = document.getElementById('colorCanvas');
var colorContext = colorCanvas.getContext('2d');
colorCanvas.style.cursor = "url('./img/cursor_colorpicker.png'), pointer";
initColorPicker();
function initColorPicker() {
    var image = new Image(250, 250);
    image.onload = () => colorContext.drawImage(image, 0, 0, image.width, image.height);
    image.crossOrigin = '';
    image.src = "https://i.imgur.com/ikt6NUI.png";
}
colorCanvas.addEventListener('mousedown', function (e) {
    var colorRect = colorCanvas.getBoundingClientRect();
    var colorData = colorContext.getImageData(e.clientX - colorRect.left, e.clientY - colorRect.top, 1, 1);
    var rgba = colorData.data;

    color = "rgb(" + rgba[0] + "," + rgba[1] + "," + rgba[2] + ")";
})
